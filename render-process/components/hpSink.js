let configuration = require('../../configuration');
let utils = require('../../utils');

class HpBoard {
    constructor(side) {
        this.side = side;
        this.value = 0;
        this.width = configuration.get('components.powerSink.width');
        this.height = configuration.get('components.powerSink.width') * 12;
        this.__calculateX();
        this.__calculateY();
        this.animation = null;
    }

    __calculateX() {
        let thumbWidth = configuration.get('components.card.thumb.width');
        let thumbHeight = configuration.get('components.card.thumb.height');
        let a = thumbWidth > thumbHeight ? thumbWidth : thumbHeight;
        let leftSideBarWidth = configuration.get('components.leftSideBar.width');
        let hMargin = configuration.get('components.slot.hMargin');
        let leftSideBarMarginRight = configuration.get('components.leftSideBar.marginRight');
        let powerSinkWidth = configuration.get('components.powerSink.width');

        this.x = leftSideBarWidth + leftSideBarMarginRight + thumbWidth + (hMargin + a) * 5 +
            hMargin + thumbWidth + hMargin + powerSinkWidth + hMargin + powerSinkWidth +
            hMargin + hMargin + hMargin + this.width / 2;
    }

    __calculateY() {
        let vMargin = configuration.get('components.slot.vMargin');
        if (this.side == 'bottom') {
            this.y = configuration.get('window.height') - vMargin * 2 - this.height / 2;
        } else {
            this.y = vMargin + this.height / 2;
        }
    }

    onHpChange(sender, args) {
        let side = args.side;
        if (this.side == side) {
            let finalHp = args.value;
            this.animation = {
                operation: 'changeHp',
                value: finalHp,
                originalValue: this.value,
                start: Date.now(),
                period: 0.5 * 1000,
                cb: args.cb
            };
        }
    }

    draw(canvas) {
        if (this.animation) {
            let percentage = (Date.now() - this.animation.start) / this.animation.period;
            if (this.animation.operation === 'changeHp') {
                let finalHp = this.animation.value;
                if (percentage > 1) {
                    this.value = finalHp;
                } else {
                    this.value = this.animation.originalValue +
                        parseInt((finalHp - this.animation.originalValue) * percentage);
                }
            } else throw new Error();
            if (percentage > 1) {
                let cb = this.animation.cb;
                this.animation = null;
                if (cb) {
                    cb();
                }
            }
        }
        let ctx = canvas.getContext('2d');
        ctx.save();
        let currentY = this.y - this.height / 2 + this.width / 3;
        utils.drawRectTri(ctx, this.x - this.width / 2, currentY,
            this.width, this.height - this.width / 2, this.width / 2);
        ctx.fillStyle = '#7a6642';
        ctx.fill();
        ctx.beginPath();
        const count = 12;
        for (let i = 0; i < count; ++i) {
            utils.drawRectTri(ctx, this.x - this.width / 2, currentY,
                this.width, this.width / 2, this.width / 2);
            currentY += this.height / count;
        }
        ctx.strokeStyle = '#c3a86e';
        ctx.lineWidth = 8;
        ctx.stroke();
        ctx.strokeStyle = '#c3a86e';
        ctx.lineWidth = 4;
        ctx.stroke();
        ctx.fillStyle = '#2d221a';
        ctx.fill();
        ctx.clip();
        ctx.beginPath();
        let percentage = this.value / 12000;
        if (percentage > 1) percentage = 1;
        ctx.fillStyle = 'rgb(177,17,22)';
        ctx.rect(this.x - this.width / 2, this.y + this.height * (0.5 - percentage),
            this.width, this.height);
        ctx.fill();
        ctx.restore();
        ctx.beginPath();
        ctx.textAlign = 'center';
        if (this.side === 'bottom') {
            ctx.textBaseline = 'bottom';
        } else {
            ctx.textBaseline = 'top';
        }
        ctx.fillStyle = 'rgb(177,17,22)';
        ctx.font = '60px Helvetica Helvetica';
        ctx.fillText(this.value + '', this.x,
            this.side === 'bottom' ? this.y - this.height / 2 : this.y + this.height / 2);
    }
}

module.exports = HpBoard;
