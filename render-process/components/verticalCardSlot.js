let configuration = require('../../configuration');

class VerticalCardSlot {
    constructor(type, side) {
        this.type = type;
        this.side = side;

        let thumbWidth = configuration.get('components.card.thumb.width');
        let thumbHeight = configuration.get('components.card.thumb.height');

        this.width = thumbWidth;
        this.height = thumbHeight;

        this.__calculateX();
        this.__calculateY();

        this.lastIn = -1;
    }

    init(args) {
        this.game = args.game;
    }

    includesPoint(x, y) {
        return x < this.x + this.width / 2 && x > this.x - this.width / 2
            && y < this.y + this.height / 2 && y > this.y - this.height / 2;
    }

    __calculateX() {
        let leftSideBarWidth = configuration.get('components.leftSideBar.width');
        let hMargin = configuration.get('components.slot.hMargin');
        let leftSideBarMarginRight = configuration.get('components.leftSideBar.marginRight');
        let slotA = this.width > this.height ? this.width : this.height;
        if (this.type == 'field') {
            this.x = leftSideBarWidth + leftSideBarMarginRight +
                this.width + 5 * (hMargin + slotA) + hMargin + this.width +
                hMargin + this.width / 2;
        } else {
            if (this.side == 'bottom') {
                if (this.type == 'grave' || this.type == 'out' || this.type == 'deck') {
                    this.x = leftSideBarWidth + leftSideBarMarginRight +
                        this.width + 5 * (hMargin + slotA) + hMargin + this.width / 2;
                } else if (this.type == 'ex' || this.type == 'hero') {
                    this.x = leftSideBarWidth + leftSideBarMarginRight + this.width / 2;
                } else {
                    throw new Error();
                }
            } else {
                if (this.type == 'grave' || this.type == 'out' || this.type == 'deck') {
                    this.x = leftSideBarWidth + leftSideBarMarginRight + this.width / 2;
                } else if (this.type == 'ex' || this.type == 'hero') {
                    this.x = leftSideBarWidth + leftSideBarMarginRight +
                        this.width + 5 * (hMargin + slotA) + hMargin + this.width / 2;
                } else {
                    throw new Error();
                }
            }
        }
    }

    __calculateY() {
        let vMargin = configuration.get('components.slot.vMargin');
        let windowHeight = configuration.get('window.height');
        if (this.type == 'field') {
            this.y = windowHeight / 2;
        } else {
            if (this.side == 'bottom') {
                if (this.type == 'grave' || this.type == 'hero') {
                    this.y = windowHeight - (this.height + vMargin) * (2 + 1) - vMargin + this.height / 2;
                } else if (this.type == 'ex' || this.type == 'out') {
                    this.y = windowHeight - (this.height + vMargin) * (1 + 1) - vMargin + this.height / 2;
                } else if (this.type == 'deck') {
                    this.y = windowHeight - (this.height + vMargin) - vMargin + this.height / 2;
                } else {
                    throw new Error();
                }
            } else {
                if (this.type == 'grave' || this.type == 'hero') {
                    this.y = (this.height + vMargin) * (2) + vMargin + this.height / 2;
                } else if (this.type == 'ex' || this.type == 'out') {
                    this.y = (this.height + vMargin) * (1) + vMargin + this.height / 2;
                } else if (this.type == 'deck') {
                    this.y = vMargin + this.height / 2;
                } else {
                    throw new Error();
                }
            }
        }
    }

    mouseIn() {
        this.lastIn = Date.now();
    }

    draw(canvas, params) {
        let ctx = canvas.getContext('2d');
        let r = configuration.get('components.slot.color.r');
        let g = configuration.get('components.slot.color.g');
        let b = configuration.get('components.slot.color.b');
        ctx.rect(this.x - this.width / 2, this.y - this.height / 2, this.width, this.height);
        ctx.strokeStyle = `rgb(${r}, ${g}, ${b})`;
        let alpha = 0.25;
        if (params.mouseIsIn) {
            // animation: go darker then lighter (0.25 -> 0.75 -> 0.25)
            let period = 2 * 1000;
            let totalTime = Date.now() - this.lastIn;
            let timeInCurrentPeriod = totalTime - parseInt(totalTime / period) * period;
            let percentage = timeInCurrentPeriod / period;
            let maxAlpha = 0.75;
            let minAlpha = 0.25;
            if (percentage > 0.5) {
                // going down
                percentage -= 0.5;
                alpha = maxAlpha - (maxAlpha - minAlpha) * percentage * 2;
            } else {
                // going up
                alpha = (maxAlpha - minAlpha) * percentage * 2 + minAlpha;
            }
        }
        ctx.fillStyle = `rgba(${r}, ${g}, ${b}, ${alpha})`;
        ctx.stroke();
        ctx.fill();
        if (params.mouseIsIn) {
            ctx.beginPath();
            ctx.fillStyle = configuration.get('components.slot.fontColor');
            ctx.font = configuration.get('components.slot.font');
            ctx.textBaseline = 'middle';
            ctx.textAlign = 'center';
            ctx.fillText(this.type.substring(0, 1).toUpperCase(), this.x, this.y);
        }
    }
}

module.exports = VerticalCardSlot;
