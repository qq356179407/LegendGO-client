let configuration = require('../../configuration');
let utils = require('../../utils');
let game = require('../game');

class Skill {
    constructor(card, skillInfo, index) {
        this.card = card;
        this.skillInfo = skillInfo;
        this.index = index;
        this.__calculateX();
        this.a = configuration.get('components.skill.width');
        this.y = this.a / 2;

        this.__load();
    }

    init(initConfig) {
        this.componentContainer = initConfig.componentContainer;
        this.id = initConfig.id;
        this.canvas = initConfig.canvas;
    }

    includesPoint(x, y) {
        return x < this.x + this.a / 2 && x > this.x - this.a / 2
            && y < this.y + this.a / 2 && y > this.y - this.a / 2;
    }

    __load() {
        configuration.getImage(this.skillInfo.image, image => {
            this.image = image;
        });
    }

    mousePos(x, y) {
        if (this.includesPoint(x, y)) {
            // skill desc
            let fontSize = configuration.get('components.leftSideBar.text.fontSize');
            let fontFamily = configuration.get('components.leftSideBar.text.fontFamily');
            let textMargin = configuration.get('components.leftSideBar.text.margin');
            let twh = new utils.TextWriterHelper(this.canvas.getContext('2d'),
                textMargin, fontSize, configuration.get('components.desc.width') - textMargin * 2,
                fontSize, fontFamily);
            let resultHeight = twh.calculateDrawLineHeight([this.skillInfo.name, this.skillInfo.desc]);
            game.showDescription(this.id, this.componentContainer, x, y,
                fontSize + resultHeight, (ctx, width) => {
                    // start draw
                    ctx.fillStyle = 'white';
                    let twh = new utils.TextWriterHelper(ctx,
                        textMargin, fontSize, configuration.get('components.desc.width') - textMargin * 2,
                        fontSize, fontFamily);

                    twh.writeLine(this.skillInfo.name);
                    twh.writeLine(this.skillInfo.desc);
                });
        }
    }

    mouseOut() {
        game.hideDescription(this.id);
    }

    __calculateX() {
        let leftBarWidth = configuration.get('components.leftSideBar.width');
        let eachSection = leftBarWidth / 3;
        let baseX = eachSection / 2;
        this.x = eachSection * this.index + baseX;
    }

    draw(canvas, params) {
        let ctx = canvas.getContext('2d');
        if (this.image) {
            ctx.drawImage(this.image, this.x - this.a / 2, this.y - this.a / 2, this.a, this.a);
        }
        ctx.rect(this.x - this.a / 2, this.y - this.a / 2, this.a, this.a);
        ctx.strokeStyle = 'black';
        ctx.stroke();
        let isActive = params.isActive;
        if (this.skillInfo.passive) {
            ctx.fillStyle = 'rgba(0,0,0,0.6)';
            ctx.fill();
        } else if (!isActive) {
            ctx.fillStyle = 'rgba(192,192,192,0.6)';
            ctx.fill();
        }
    }
}

module.exports = Skill;
