let handler = module.exports;
let configuration = require('../../configuration');

let game;

handler.init = function (config) {
    game = config.game;
};

handler.testPileContent = [];

function getCardsInPile(pile) {
    // TODO
    return handler.testPileContent;
}

handler.listen = function (pile) {
    let cards = getCardsInPile(pile);
    if (cards === null) {
        // cards should not be displayed
        game.showErrorMsg(configuration.get('i18n.cannotDisplayPile'), 500);
        return;
    }

    pile.showContentCards(cards);
};
