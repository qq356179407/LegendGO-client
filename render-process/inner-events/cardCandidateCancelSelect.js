let handler = module.exports;
let service = require('../inner-event-services/cardSelectionService');

let game;

handler.init = function (config) {
    game = config.game;
    service.init({
        game: game
    });
};

handler.listen = function (card) {
    // TODO
    service.deselect(card);
    console.log('cardCandidateCancelSelect');
};
