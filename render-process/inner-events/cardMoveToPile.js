let handler = module.exports;
let service = require('../inner-event-services/cardSelectionService');

let game;

handler.init = function (config) {
    game = config.game;
    service.init({
        game: game
    });
};

function getSide(card) {
    // TODO
    return 'bottom';
}

handler.listen = function (sender, params) {
    let card = params.card;
    let type = params.pile;

    let side = getSide(card);
    let self = this;
    game.componentBroadcast('requirePile', this, {
        type: type,
        side: side,
        cb: function (pile) {
            card.onCardAnimation(self, {
                compId: card.compId,
                operation: (type == 'deck' || type == 'ex') ? 'backMove' : 'straightMove',
                x: pile.x,
                y: pile.y,
                cb: function () {
                    pile.cardCount += 1;
                    pile.topCard = card;
                }
            });
        }
    });
};
